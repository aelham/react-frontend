#!/bin/sh
set -e

# Check for ENV_DATA environment variable
if [ -n "$ENV_DATA" ]
then
  # Build the env data if it exists
  yarn build:envData
  # Remove ENV_DATA from the environment
  unset ENV_DATA
  # Generate env for runtime use
  yarn react-env --env APP_ENV
  # Execute any subsequent command
  exec "$@"
else
  # Generate env for runtime use
  yarn react-env --env APP_ENV
  # Execute any subsequent command
  exec "$@"
fi
