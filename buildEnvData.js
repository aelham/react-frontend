const fs = require('fs');

const data = JSON.parse(process.env.ENV_DATA);
const envData = Object.keys(data)
  .map(key => `${key}=${JSON.stringify(data[key])}`)
  .join('\n');
fs.writeFileSync('.env', `${envData}\n`);
